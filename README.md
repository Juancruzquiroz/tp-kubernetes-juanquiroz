# TP-kubernetes-JuanQuiroz

Ejecutar un Sitio Web en Cluster Kubernetes con un despliegue del tipo Dployment.

**PROYECTO MONTAR PAGINA WEB EN KUBERNETES**
                                                                                                                        Juan De La cruz Quiroz
**Instalado Kubernetes Microk8s en ubunutu**

**1-Generar el Deployment de la APP utilizando fichero .yaml**
-Crear fichero nginx-dp.yaml
$ vim nginx-dp.yaml # Fichero deployment
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-dp
  namespace: default
  labels:
    app: nginx
spec:
  revisionHistoryLimit: 2
  strategy:
    type:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx:stable-alpine
        name: nginx
        ports:
        - name: http
          containerPort: 80
-	Crear el deployment
 $ kubectl create -f nginx-dp.yaml
-Verifico que se creó y examinamos el detalle
$ kubectl get pods
$ kubectl describe pod nginx-dp-55d44446b8-nvtbz

Con la IP que detalla en al paso anterior se puede verificar que el pod esta ecuchendo en el puerto 80 dentro del cluster. Remplazar las x por la IP del POD
$ curl http://x.x.x.x:80

Modificar la cantidad de Replicas
$ kubectl scale deployment nginx-dp --replicas 3

Revisar que se actualizo el estado de las replicas con el siguiente comando o editar el fichero nginx-pd.yaml
$ kubectl get pods -l app=nginx

**2-Para publicar la app y que sea accesible desde afuera se de definir un Service.**

-Se crea el fichero nginx-sv.yaml
$vim nginx-sv.yaml

> apiVersion: v1
kind: Service
metadata:
  name: nginx-sv
  labels:
    app: nginx
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
  type: LoadBalancer
> 
-Crear el Service
$ kubectl apply -f nginx-sv.yaml

-Verificamos que se haya creado
$ kubectl describe service nginx-sv

-Como no se especificó ningún Puerto de destino Kubernetes asigna uno libre al azar y con al comando ejecutado en el paso anterior podemos ver el puerto asignado.
-Si se accede a la IP del clúster a través del puerto asignado, Kubernetes redirigirá la petición al puerto 80 del servicio, con lo cual obtengo la página de bienvenida de Nginx.

**3- El proyecto está incompleto falta subir nuestra página web a la infraestructura generada en kubernetes**
